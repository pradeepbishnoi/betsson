Solution by Pradeep Bishnoi
===================



Section A � Simple Programming Task
-------------

Describe an algorithm which sums all numbers from 1 to 1000000 both numbers inclusive in the simplest possible way.

#####  **Answer**


We can use the Gauss Algorithm to get the sum of 1 to N natural numbers.  

```
sum = n*(n+1)/2
```

```
sum = 1000000*(1000000+1)/2 = 500000500000
```


 **If using formula is not allowed, then here is the Algorithm:**

> - Set variable endDigit

        set endDigit=1000000
> - Check if endDigit is either **EVEN or ODD number**. 

    if EVEN, set startDigit=1
    if ODD, set startDigit=0
> - Divide the endDigit by 2 and store into **countTill**. 

    countTill = endDigit/2
> - Set resultVariable to ZERO

    set resultVariable = 0
> - Start a loop from **startDigit** and till **countTill**.
> - With each iteration, store the SUM of **resultVariable**, **startDigit** and **endDigit** into  **resultVariable**. Also **increment startDigit by 1** and **decrement endDigit by 1** 

    resultVariable = resultVariable + startDigit + endDigit
    startDigit = startDigit + 1
    endDigit = endDigit - 1


----------

Section B � Real Life Case
-------------


#### Case 1: Automation

> **INFO:** The shared soapui project XML is written in soapUI Open Source version. Hence it involve lot of Groovy code and custom (file based) datasource, dataloop teststep. It has INFO and DEBUG level logging also added.

Project is mainly divided into 2 parts: **Data Generator** and **Data Tester**. 

**Data Generator** is meant to be one time activity for creating the required test data.
> - Obtain the city list, http://openweathermap.org/help/city_list.txt
> - Store the downloaded .txt file and convert it into .csv file
> - Used Country Alpha Code (from the csv) and got the respective Currency, Nationality etc information using the API https://restcountries.eu/rest/v2/alpha/{code}
> - Store the obtained information temporary in some Property variable
> - Parse the cityList csv file and generate random user data (all mentioned fields), put the other required field information using the temporary stored data (currency, nationality). And save it in final dummyUser csv file.

**Data Tester** is meant to be used for performing active testing on the generated test data.

> - We can clone the TestCase (named DataSet) and acheive parallel test execution.
> - Each TestCase need to specify the starting record number (has Groovy script which will read the final dummyUser csv file).
> - Contains 3 RestRequest TestStep
>   - One to obtain the current weather information for the city,country combination
>     - Second to obtain the Currency conversion information (using Xignite API) and split the received (in Euro) annual gross income to Weekly income.
>     -   Third, it the weather condition for Today and Tommorow is rainy, then we call the Historical Weather API to obtain Yesterday weather information for that city. If that is also Rainy, it suggest the recommended City to visit (with favourable sunny weather).

> **PS:** While running the test, if the current weather is favourable condition (i.e., sunny) then we add it into our list of recommended city. **Other approach**, could have  been using some API which provide us the city name list in response, when provided with current weather condition as input.



#### Case 2: Performance




> ***Confused with the wording used in this question. Seeking clarification over email.***


----------
